# pandoc

Ansible role that installs latest release of [pandoc](https://github.com/jgm/pandoc), and 
installs [Wandmalfarbe's pandoc-latex-template](https://github.com/Wandmalfarbe/pandoc-latex-template).
